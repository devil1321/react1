import React from 'react';

const ListItem = (props) => {
    return (
        <li className="list-item">{props.dish}</li>
    );
}

export default ListItem;    
