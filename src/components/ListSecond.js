import React,{Component} from 'react'
import ListItem from './ListItem'
export default class ListSecond extends Component {
    constructor(props) {
        super()
        this.state = {
            dishes: [
                "Pierogi",
                "Kotlet Schabowy",
                "Karkowka",
                "Sos Grzybowy",
                "Danie Kebab",
            ]
        }
    }
    render(){
        return(
            <div className="list-wrapper">
                 <h1 className="list-title">{this.props.title}</h1>
                 <ol className="list">
                    {this.state.dishes.map(dish => {
                         return <ListItem key={dish} dish={dish} />
                     })}
                 </ol>
             </div>
        )
    }
}
