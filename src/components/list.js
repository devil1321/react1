import React, { Component } from 'react'
// const List = (props) => {
//     const dishes = [
//         "Pierogi",
//         "Kotlet Schabowy",
//         "Karkowka",
//         "Sos Grzybowy",
//         "Danie Kebab",
//     ];
//     return (
//         <div className="list-wrapper">
//             <h1 className="list-title">{props.title}</h1>
//             <ol className="list">
//                 {dishes.map(dish => {
//                     return <li key={dish} className="list-item" >{dish}</li>;
//                 })}
//             </ol>
//         </div>
//     );
// };
// export default List;

export default class List extends Component {
    constructor(props) {
        super()
        this.state = {
            dishes: [
                "Pierogi",
                "Kotlet Schabowy",
                "Karkowka",
                "Sos Grzybowy",
                "Danie Kebab",
            ]
        }
    }
    render(){
        return(
            <div className="list-wrapper">
                 <h1 className="list-title">{this.props.title}</h1>
                 <ol className="list">
                    {this.state.dishes.map(dish => {
                         return <li key={dish} className="list-item">{dish}</li>;
                     })}
                 </ol>
             </div>
        )
    }
}

