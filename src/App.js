import './App.css';
import List from './components/list'
import ListSecond from './components/ListSecond'
function App() {
  return (
    <div className="App-header">
      <List title="Favoruite Dishes"/>
      <ListSecond title="Favoruite Dishes 2"/>
    </div>
  );
}

export default App;
